"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""
from functions.classes import LegoComponent


def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")


def kpi_total_cost(*args):
    """
    Calculates the sum of one or more integers or floatsor lists.

    Args:
        *args: One or more integers or floats or lists.

    Returns:
        total (float): The sum of all given integers or floats and/or the sum
        of all items in all given lists.

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than int, float or list).
    """
    total = 0
    for arg in args:
        if isinstance(arg, (int, float)):  # Check if argument is an integer or a float
            total += arg
        elif isinstance(arg, list):  # Check if argument is a list
            total += sum(arg)
        else:
            raise TypeError(
                f"Unsupported type {type(arg)} passed to kpi_sum()")
    return total


def kpi_delivery_time(*args):
    """
    Calculates the max number of one or more integers or lists.

    Args:
        *args: One or more integers or lists.

    Returns:
        total (int): The max of all given integers and/or the max
        of all items in all given lists.

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than int or list).
    """
    total = 0
    for arg in args:
        if isinstance(arg, int):  # Check if argument is an integer
            total = max([total,arg])
        elif isinstance(arg, list):  # Check if argument is a list
            total = max(arg)
        else:
            raise TypeError(
                f"Unsupported type {type(arg)} passed to kpi_sum()")
    return total


def kpi_pp_ratio(total_cost, motor):
    """
    Calculates the price to performance ratio of the car.

    Args:
        total_cost (float): total cost of the car in Euros
        motor (LegoComponent): used Lego motor

    Returns:
        total (float): The price to performacne ratio

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than int or list).
    """
    total = 0
    if not isinstance(motor, LegoComponent):  # Check if motor is a Lego Component
        raise TypeError(
            f"Unsupported type {type(arg)} passed to kpi_sum()")
    if not isinstance(total_cost, (int, float)):  # Check if total_cost is an int or float
        raise TypeError(
            f"Unsupported type {type(arg)} passed to kpi_sum()")
    v = motor.properties.get('input voltage [V]')
    i = motor.properties.get('idle current [mA]')
    total = (v*i)/total_cost
        
    return total


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
