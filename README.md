# Lerneinheit FAIRe Qualitäts-KPIs

## Einführung
Siehe Aufgabenstellung in Moodle.

## Materialien
Die Aufgabenstellung im PDF-Format steht in Moodle bereit.

In diesem GitLab Repo finden Sie:
-  Package *functions* (`functions/`): Beinhaltet die Module *classes* und *calculation_rules*
- Modul classes (`functions/classes.py`): Werkzeuge zum Aufbau und zur Bearbeitung der LEGO Konstruktionen
- Modul calculation_rules (`functions/calculation_rules.py`): Funktionen zum Berechnen der FAIR Quality KPIs
- Datenblätter (`datasheets/`): Datenblätter ausgewählter LEGO Komponenten im JSON-Format
- Jupyter Notebook (`ausarbeitung.ipynb`): Zur Bearbeitung der Aufgaben und Abgabe. Beinhaltet ein Minimalbeispiel zur Verwendung der Werkzeuge.


## Ausarbeitung
Die Ausarbeitung erfolgt im Notebook `ausarbeitung.ipynb`. In diesem ist bereits eine Gliederung vorgegeben.

## Abgabe
Die Abgabe erfolgt über moodle.
